//
//  BookTableViewController.swift
//  GoogleBook
//
//  Created by Grégoire Jacquin on 24/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import UIKit

class BookTableViewController: UITableViewController {
    var bookManager = BookManager()
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bookManager.bookCount
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookCell", for: indexPath)
        let book = bookManager.getBook(at: indexPath.row)
        cell.textLabel?.text = book.title
        cell.detailTextLabel?.text = book.author
        cell.imageView?.image = book.cover
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow,
            let bookViewController = segue.destination as? BookViewController {
            bookViewController.book = bookManager.getBook(at: selectedIndexPath.row)
            bookViewController.delegate = self
        } else if let navController = segue.destination as? UINavigationController {
            if let bookViewController = navController.topViewController as? BookViewController {
                bookViewController.delegate = self
            }
        }
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            bookManager.deleteBook(at: indexPath.row)
            tableView.reloadData()
        }
    }
    @IBAction func changeSegment(_ sender: UISegmentedControl) {
        guard let sortOrder = SortOrder(rawValue: sender.selectedSegmentIndex) else { return }
        bookManager.sortOrder = sortOrder
        tableView.reloadData()
    }
}
extension BookTableViewController: BookViewControllerDelegate {
    func saveBook(book: Book) {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            bookManager.updateBook(at: selectedIndexPath.row, with: book)
        } else {
            bookManager.addBook(book: book)
        }
        tableView.reloadData()
    }
}
