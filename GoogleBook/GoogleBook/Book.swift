//
//  Book.swift
//  GoogleBook
//
//  Created by Grégoire Jacquin on 29/07/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import Foundation
import UIKit

class Book {
    static let defaultCover = UIImage(named: "book.jpg")!
    var title: String
    var author: String
    var isbn: String
    var rating: Int
    var notes: String
    private var image: UIImage? = nil
    var cover: UIImage {
        get {
            return image ?? Book.defaultCover
        }
        set {
            image = newValue
        }
    }

    init(title: String, author: String, isbn: String, rating: Int,notes: String, cover: UIImage? = nil) {
        self.title = title
        self.author = author
        self.isbn = isbn
        self.rating = rating
        self.notes = notes
        self.image = cover
    }
}
