//
//  File.swift
//  GoogleBook
//
//  Created by Grégoire Jacquin on 05/06/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import UIKit

extension UIView {
    var firstResponder: UIView? {
        if self.isFirstResponder {
            return self
        }
        for view in self.subviews {
            if let firstResponder = view.firstResponder {
                return firstResponder
            }
        }
        return nil
    }
}
extension UIViewController {
    var firstResponder: UIView? {
        return view.firstResponder
    }
}
