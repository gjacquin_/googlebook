//
//  BookManager.swift
//  GoogleBook
//
//  Created by Grégoire Jacquin on 29/07/2019.
//  Copyright © 2019 Grégoire Jacquin. All rights reserved.
//

import Foundation
enum SortOrder: Int {
    case title
    case author
}
class BookManager {
    lazy private var books = loadBook()
    private func loadBook() -> [Book] {
        return sampleBooks()
    }
    var sortOrder: SortOrder = .title {
        didSet {
            sort(books: &books)
        }
    }
    private func sampleBooks() -> [Book] {
        var books = [
            Book(title: "Great Expectations", author: "Charles Dickens", isbn: "9780140817997", rating: 5, notes: "🎁 from Papa"),
            Book(title: "Don Quixote", author: "Miguel De Cervantes", isbn: "9788471890153", rating: 4, notes: ""),
            Book(title: "Robinson Crusoe", author: "Daniel Defoe", isbn: "", rating: 5, notes: ""),
            Book(title: "Gulliver's Travels", author: "Jonathan Swift", isbn: "", rating: 5, notes: ""),
            Book(title: "Emma", author: "Jane Austen", isbn: "", rating: 5, notes: ""),
            Book(title: "To Kill a Mockingbird", author: "Harper Lee", isbn: "", rating: 5, notes: ""),
            Book(title: "Animal Farm", author: "George Orwell", isbn: "", rating: 4, notes: ""),
            Book(title: "Gone with the Wind", author: "Margaret Mitchell", isbn: "", rating: 5, notes: ""),
            Book(title: "The Fault in Our Stars", author: "John Green", isbn: "", rating: 5, notes: ""),
            Book(title: "The Da Vinci Code", author: "Dan Brown", isbn: "", rating: 5, notes: ""),
            Book(title: "Les Misérables ", author: "Victor Hugo", isbn: "", rating: 5, notes: ""),
            Book(title: "Lord of the Flies ", author: "William Golding", isbn: "", rating: 5, notes: ""),
            Book(title: "The Alchemist", author: "Paulo Coelho", isbn: "", rating: 5, notes: ""),
            Book(title: "Life of Pi", author: "Yann Martel", isbn: "", rating: 5, notes: ""),
            Book(title: "The Odyssey", author: "Homer", isbn: "", rating: 5, notes: "")
        ]
        sort(books: &books)
        return books
    }
    func getBook(at index: Int) -> Book {
        return books[index]
    }
    var bookCount: Int {
        return books.count
    }
    func addBook(book: Book) {
        books.append(book)
        sort(books: &books)
    }
    func updateBook(at index: Int, with book: Book) {
        books[index] = book
        sort(books: &books)
    }
    func deleteBook(at index: Int) {
        books.remove(at: index)
    }
    func sort(books: inout [Book]) {
        switch sortOrder {
        case .title:
            return books.sort(by: {
                ($0.title.localizedLowercase, $0.author.localizedLowercase) < ($1.title.localizedLowercase, $1.author.localizedLowercase)
            })
        case .author:
            return books.sort(by: {
                ($0.author.localizedLowercase, $0.title.localizedLowercase) < ($1.author.localizedLowercase, $1.title.localizedLowercase)
            })
        }
       
    }
}
